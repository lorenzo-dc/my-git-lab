# frozen_string_literal: true

require 'middleman'

#
# This middleman extension is used to speed up builds on CI
#
# We effectively do this by leveraging our CI parallelism features and split the build into five
# distinct partitions:
#
# 1. `PROXY_RESOURCE`: We leverage the `proxy` feature of middleman to generate e.g. job pages,
#    direction pages and others
# 2. `IMAGES`: We have a lot of images. So we let middleman copy them. Maybe we can de-middleman
#    it in the future
# 3. `BLOG_POST`: All blog posts
# 4. `HANDBOOK`: All handbook pages
# 5. `ALL_OTHERS`: All pages and files which do not fit into the categories above
#
# If `CI_NODE_INDEX` and `CI_NODE_TOTAL` are not set, e.g. on development machines, or turning
# parallelism off, it will simply be a noop extension
class PartialBuild < Middleman::Extension
  PROXY_RESOURCE = "proxy resources"
  IMAGE = "images"
  BLOG_POST = "blog posts"
  HANDBOOK = "handbook"
  ALL_OTHERS = "all other pages"

  # We must ensure that this extension runs last, so that
  # the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def_delegator :@app, :logger

  def initialize(app, options_hash = {}, &block)
    super
    @enabled = ENV['CI_NODE_INDEX'] && ENV['CI_NODE_TOTAL']
    return unless @enabled

    raise "PartialBuild: If you want to enable parallel builds, please use exactly 5 parallel jobs" unless ENV['CI_NODE_TOTAL'].to_i == 5

    @partial = case ENV['CI_NODE_INDEX']
               when "1"
                 PROXY_RESOURCE
               when "2"
                 IMAGE
               when "3"
                 BLOG_POST
               when "4"
                 HANDBOOK
               when "5"
                 ALL_OTHERS
               else
                 raise "PartialBuild: Invalid Build Partial #{ENV['CI_NODE_INDEX']}. At the moment we only support 1 to 5"
               end
  end

  def image?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?('images/')
  end

  def handbook?(resource)
    !proxy_resource?(resource) &&
      resource.destination_path.start_with?('handbook/')
  end

  def blog_page?(resource)
    !proxy_resource?(resource) &&
      (resource.destination_path.start_with?('blog/') ||
        resource.destination_path.end_with?('atom.xml'))
  end

  def proxy_resource?(resource)
    resource.instance_of?(Middleman::Sitemap::ProxyResource) ||
      resource.destination_path.start_with?('templates/', 'direction/', 'sales/') ||
      resource.destination_path.end_with?('/template.html', 'category.html')
  end

  def all_others?(resource)
    !proxy_resource?(resource) &&
      !blog_page?(resource) &&
      !image?(resource) &&
      !handbook?(resource)
  end

  def part_of_partial?(resource)
    return true if resource.destination_path == 'sitemap.xml'

    case @partial
    when PROXY_RESOURCE
      proxy_resource?(resource)
    when IMAGE
      image?(resource)
    when BLOG_POST
      blog_page?(resource)
    when HANDBOOK
      handbook?(resource)
    when ALL_OTHERS
      all_others?(resource)
    else
      raise "PartialBuild: You are trying to build a unknown partial: #{@partial}"
    end
  end

  def manipulate_resource_list(resources)
    unless @enabled
      logger.info "PartialBuild: We are not building a partial build, building everything"
      return resources
    end

    logger.info "PartialBuild: We are building the partial: #{@partial}"

    resources.select { |resource| part_of_partial?(resource) }
  end
end

::Middleman::Extensions.register(:partial_build, PartialBuild)
