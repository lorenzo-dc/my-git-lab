---
layout: handbook-page-toc
title: "SYS.1.01 - Audit Logging Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SYS.1.01 - Audit Logging

## Control Statement

GitLab logs critical information system activity.

## Context

Logging is the foundation for a variety of other security controls including monitoring, incident response, and configuration management. Without comprehensive and reliable logs, large parts of our security compliance program wouldn't be possible. This control is left vague by design. As we develop our system maps and inventories this control will likely become a bit more targeted. To start we really want all GitLab teams to enable system-level logging on all production systems.

An auditor will look to validate in-scope systems are generating logs, those logs are collected, retained the required amount of time and utilized to monitor for performance, health, and anomalies.  To validate the control is working properly, the auditor should require additional pieces of information to demonstrate audit logging is functioning properly.  Those information items include:

* Review of the audit and accountability policy and procedures
* Confirmation that audit events are reviewed and updated on a recurring basis
* Review what should be collected for auditing and cross-reference against what is collected
* Determine what defines a production system to validate the correct systems are being audited
* Confirm log validation processes are working as intended
* Master asset listing to confirm correct systems are being audited
* Log collection process(es)

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control Owner: 

* Infrastructure

Process Owners:

* Security Compliance
* Infrastructure
* Security Operations

## Guidance

Server configuration standards should have logging information enabled for each type of system.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Audit Logging control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/904).

### Policy Reference

## Framework Mapping

* ISO
  * A.12.4.1
* SOC2 CC
  * CC7.2
* NIST 800-53
  * AU-1 
  * AU-2
  * AU-9
  * AU-12
